-module(repose_record).
-export([start/0]).

-record(timestamp, {date, minute, info}).
-record(guard, {guard_num, sleep_total=0, sleeping_minutes_list=[]}).

start() ->
    {Data, Guard_Count} = read_file("2018_day_4_input.txt"),
    ets:new(guard_table, [ordered_set, named_table, {keypos, #guard.guard_num}]),
    populate_table(Guard_Count),
    {Sleepiest_Guard, Minute_Most_Asleep} = sum_sleeping_time(0, Data),
    Part_1 = Sleepiest_Guard#guard.guard_num * Minute_Most_Asleep,
    io:format("Part 1: ~w~n", [Part_1]),
    {Sleepiest_Guard2, _, Minute_Most_Asleep2} = find_guard_with_minute_most_asleep(),
    Part_2 = Sleepiest_Guard2 * Minute_Most_Asleep2,
    io:format("Part 2: ~w~n", [Part_2]),
    ets:delete(guard_table).

read_file(Filename) ->
    case file:open(Filename, [read]) of
        {ok, Device} -> try get_all_lines(Device, [], 0) of
                            Data -> Data
                        after file:close(Device)
                        end;
        {error, _} -> error
    end.

get_all_lines(Device, List, Guard_Count) ->
    case io:get_line(Device, "") of
        eof  ->
            Ordered_List = lists:sort(fun(A, B) -> A#timestamp.date =< B#timestamp.date end,
                                      List),
            {Ordered_List, Guard_Count};
        Line ->
            Stripped_Line = string:trim(Line),
            {match, Data_Values} = re:run(Stripped_Line,
                                          ".(.*)\-(.*)\-(.*)\s(.*):(.*).\s(.*)$|.(.*)\-(.*)\-(.*)\s(.*):(.*).\s(.*)$",
                                          [{capture, [1, 2, 3, 4, 5, 6], list}, ungreedy]),
            {Date, _} = string:to_integer(lists:nth(1, Data_Values) ++ lists:nth(2, Data_Values)
                                     ++ lists:nth(3, Data_Values) ++ lists:nth(4, Data_Values)
                                     ++ lists:nth(5, Data_Values)),
            {Minute, _}   = string:to_integer(lists:nth(5, Data_Values)),
            case re:run(lists:nth(6, Data_Values),
                        "Guard\s#(.*)\s.*$",
                        [{capture, [1], list}, ungreedy]) of
                {match, Info_String} ->
                    {Info, _} = string:to_integer(Info_String),
                    if
                        Guard_Count < Info -> Next_Guard_Count = Info;
                        true -> Next_Guard_Count = Guard_Count
                    end;
                nomatch ->
                    Info = lists:nth(6, Data_Values),
                    Next_Guard_Count = Guard_Count
            end,
            Timestamp = #timestamp{date = Date,
                                   minute = Minute,
                                   info   = Info},
            get_all_lines(Device, [Timestamp|List], Next_Guard_Count)
    end.


%% Part 1
populate_table(Count) ->
    Guard_Count_List = lists:seq(1, Count),
    List_Of_Guards = lists:map(fun(X) -> #guard{guard_num=X} end, Guard_Count_List),
    ets:insert(guard_table, List_Of_Guards).

sum_sleeping_time(Guard_Number, [H|T]) ->
    case erlang:is_integer(H#timestamp.info)  of
        true  ->
            Next_Guard_Number = H#timestamp.info,
            sum_sleeping_time(Next_Guard_Number, T);
        false ->
            Fall_Asleep = H,
            [Wake_Up|T2] = T,
            Time_Asleep = Wake_Up#timestamp.minute - Fall_Asleep#timestamp.minute,
            Sleeping_Minutes_List = lists:seq(Fall_Asleep#timestamp.minute, Wake_Up#timestamp.minute - 1),
            Stored_Guard = erlang:hd(ets:lookup(guard_table, Guard_Number)),
            ets:insert(guard_table, #guard{guard_num = Guard_Number,
                                           sleep_total = Stored_Guard#guard.sleep_total + Time_Asleep,
                                           sleeping_minutes_list = Stored_Guard#guard.sleeping_minutes_list ++ Sleeping_Minutes_List}),
            sum_sleeping_time(Guard_Number, T2)
    end;
sum_sleeping_time(_, []) ->
    Guard = #guard{guard_num=0},
    Sleepiest_Guard = ets:foldl(fun(X, Acc) -> if
                                                   X#guard.sleep_total > Acc#guard.sleep_total ->
                                                       X;
                                                   true ->
                                                       Acc
                                               end
                                end, Guard, guard_table),
    Sleeping_Minutes_List = lists:sort(Sleepiest_Guard#guard.sleeping_minutes_list),
    {_, Minute_Most_Asleep} = find_minute_most_asleep(Sleeping_Minutes_List),
    {Sleepiest_Guard, Minute_Most_Asleep}.

find_minute_most_asleep([]) ->
    {0, 0};
find_minute_most_asleep(Sleeping_Minutes_List) ->
    [H|T] = Sleeping_Minutes_List,
    find_minute_most_asleep(T, [], {1, H}).

find_minute_most_asleep([H|T], Freq_List, {Freq, Min}) ->
    case H == Min of
        true  -> find_minute_most_asleep(T, Freq_List, {Freq+1, Min});           
        false -> find_minute_most_asleep(T, [{Freq, Min}|Freq_List], {1, H})
    end;
find_minute_most_asleep([], Freq_List, {Freq, Min}) ->
    Sorted_Freq_List = lists:sort(fun(A, B) -> {A2, _}=A, {B2, _}=B, A2=<B2 end, [{Freq, Min}|Freq_List]),
    {Freq_Of_Minute_Most_Slept, Minute_Most_Asleep} = lists:last(Sorted_Freq_List),
    {Freq_Of_Minute_Most_Slept, Minute_Most_Asleep}.

%% Part 2
find_guard_with_minute_most_asleep() ->
    find_guard_with_minute_most_asleep(ets:first(guard_table), []).

find_guard_with_minute_most_asleep('$end_of_table', Minute_Most_Asleep_Per_Guard_List) ->
    Sorted_Freq_List = lists:sort(fun(A, B) -> {_, A2, _}=A, {_, B2, _}=B, A2=<B2 end, Minute_Most_Asleep_Per_Guard_List),
    Stupid = lists:last(Sorted_Freq_List),
    Stupid;
find_guard_with_minute_most_asleep(Guard_Key, Minute_Most_Asleep_Per_Guard_List) ->
    Guard = erlang:hd(ets:lookup(guard_table, Guard_Key)),
    Sleeping_Minutes_List = lists:sort(Guard#guard.sleeping_minutes_list),
    {Freq, Minute_Most_Asleep} = find_minute_most_asleep(Sleeping_Minutes_List),
    find_guard_with_minute_most_asleep(ets:next(guard_table, Guard#guard.guard_num),
                                       [{Guard#guard.guard_num, Freq, Minute_Most_Asleep}|Minute_Most_Asleep_Per_Guard_List]).

