-module(the_sum_of_its_parts).

-export([start/0]).

start() ->
    Steps = read_file("2018_day_7_input.txt"),
%    Steps = read_file("test.txt"),
    Part_1 = build_steporder_list(Steps),
    Part_2 = calculate_assembly_time(Steps),
    io:format("Part 1: ~s~n", [Part_1]),
    io:format("Part 2: ~w~n", [Part_2]).

read_file(Filename) ->
    case file:open(Filename, [read]) of
        {ok, Device} -> try get_all_lines(Device, []) of
                            Data -> Data
                        after file:close(Device)
                        end;
        {error, _} -> error
    end.

get_all_lines(Device, List) ->
    case io:get_line(Device, "") of
        eof  ->
            lists:reverse(List);
        Line ->
            Stripped_Line = string:trim(Line),
            {match, [Step_One, Step_Two]} = re:run(Stripped_Line,
                                                   "Step\s(.)\smust\sbe\sfinished\sbefore\sstep\s(.)\scan\sbegin\.$",
                                                   [{capture, [1, 2], list}, ungreedy]),
            get_all_lines(Device, [{Step_One, Step_Two}|List])
    end.

%% Part 1
build_steporder_list(Steps) ->
    build_steporder_list(Steps, []).

build_steporder_list([{Step1, Step2}], Order_List) ->
    lists:reverse([Step2|[Step1|Order_List]]);
build_steporder_list(Steps, Order_List) ->
    Step2_List = lists:foldl(fun(Elem, Step2_List) ->
                                     {_,Step2} = Elem,
                                     [Step2|Step2_List]
                             end, [], Steps),
    Avail = lists:filter(fun(Elem) ->
                                 {Step1, _} = Elem,
                                 case lists:member(Step1, Step2_List) of
                                     true  -> false;
                                     false -> true
                                 end
                         end, Steps),
    Avail_Step1_List = lists:sort(
                         lists:foldl(fun(Elem, AccList) ->
                                             {Step1, _} = Elem,
                                             [Step1|AccList]
                                     end, [], Avail)),
    First_Avail_Step1 = erlang:hd(Avail_Step1_List),
    New_Step_List = lists:filter(fun(Elem) ->
                                         {Step1, _} = Elem,
                                         case Step1 == First_Avail_Step1 of
                                             true -> false;
                                             false -> true
                                         end
                                 end, Steps),
    build_steporder_list(New_Step_List, [First_Avail_Step1|Order_List]).

%% Part 2
calculate_assembly_time(Steps) ->
    calculate_assembly_time([{none, 0}, {none, 0}, {none, 0}, {none, 0}, {none, 0}], 0, Steps).

calculate_assembly_time(_Worker_List, Time_Sum, [{Step1, Step2}]) ->
    Alphabet = lists:seq($A, $Z),
    Time_Sum + 60 + string:str(Alphabet, [erlang:hd(Step1)]) + 60 + string:str(Alphabet, [erlang:hd(Step2)]);
calculate_assembly_time(Worker_List, Time_Sum, Steps) ->
    Alphabet = lists:seq($A, $Z),
    Step2_List = lists:foldl(fun(Elem, Step2_List) ->
                                     {_,Step2} = Elem,
                                     [Step2|Step2_List]
                             end, [], Steps),
    Avail = lists:filter(fun(Elem) ->
                                 {Step1, _} = Elem,
                                 case lists:member(Step1, Step2_List) of
                                     true  -> false;
                                     false -> true
                                 end
                         end, Steps),
    Avail_Step1_List = lists:filter(fun(Elem) -> 
                                            Res = lists:search(fun({Step, _}) ->
                                                                       Step == Elem
                                                               end, Worker_List),
                                            if
                                                Res == false -> true;
                                                true -> false
                                            end
                                    end, 
                                    lists:usort(
                                      lists:foldl(fun(Elem, AccList) ->
                                                          {Step1, _} = Elem,
                                                          [Step1|AccList]
                                                  end, [], Avail))),
    Busy_Workers = lists:filter(fun({_, Time_Left}) ->
                                        if
                                            Time_Left == 0 -> false;
                                            true -> true
                                        end
                                end, Worker_List),
    Nr_Avail_Workers = 5 - erlang:length(Busy_Workers),
    Newly_Assigned_Workers = lists:map(fun(Step) ->
                                               {Step, 60 + string:str(Alphabet, [erlang:hd(Step)])}
                                       end, lists:sublist(Avail_Step1_List, 1, Nr_Avail_Workers)),
    New_Worker_List = lists:map(fun({Step, Time_Left}) ->
                                        {Step, Time_Left-1}
                                end, lists:append(Busy_Workers, Newly_Assigned_Workers)),
    New_Step_List = lists:foldl(fun({Step, Time_Left}, Acc) ->
                                        case Time_Left == 0 of
                                            true -> 
                                                lists:filter(fun(Elem) ->
                                                                     {Step1, _} = Elem,
                                                                     case Step1 == Step of
                                                                         true -> false;
                                                                         false -> true
                                                                     end
                                                             end, Acc);
                                            false -> 
                                                Acc
                                        end
                                end, Steps, New_Worker_List),
    calculate_assembly_time(New_Worker_List, Time_Sum+1, New_Step_List).

