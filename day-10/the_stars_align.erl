-module(the_stars_align).
-export([start/0]).

start() ->
    Data = read_file("2018_day_10_input.txt"),
    part_1(Data).

%% Read data from file
read_file(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Data2 = binary:split(Data, [<<"\n">>], [global]),
    [format_data(L) || L <- remove_blank(Data2)].

format_data(Line) ->
    Stripped_Line = binary_to_list(Line),
    {match, Data} = re:run(Stripped_Line,
                           "position=<\s*([-]?\\d+),\s*([-]?\\d+)>\svelocity=<\s*([-]?\\d+),\s*([-]?\\d+)>$",
                           [{capture, [1, 2, 3, 4], list}, ungreedy]),
    {X, _} = string:to_integer(lists:nth(1, Data)),
    {Y, _} = string:to_integer(lists:nth(2, Data)),
    {DX, _} = string:to_integer(lists:nth(3, Data)),
    {DY, _} = string:to_integer(lists:nth(4, Data)),
    {{X, Y}, {DX, DY}}.

remove_blank(List) ->
    lists:filter(fun(El) -> El /= <<>> end, List).

%% Part 1
part_1(Data) ->
    simulate_next_second(Data, 0).

simulate_next_second(Points, Second) ->
    case determine_display_points(Points) of
        finished ->
            %% Part 2
            io:format("Seconds: ~p~n", [Second]);
        continue ->
            UpdatedPoints = lists:map(fun move_point/1, Points),
            simulate_next_second(UpdatedPoints, Second+1)
    end.

move_point({{X, Y}, {DX, DY}=Velocity}) ->
    {{X+DX, Y+DY}, Velocity}.

determine_display_points(Points) ->
    PointsXY = lists:map(fun({Point, _}) -> Point end, Points),
    ScaleAdjustedPoints = scale_adjust_points(PointsXY),
    {XMax, YMax} = find_max_xy(ScaleAdjustedPoints),
    case YMax < 10 of
        true  ->
            display_points({XMax, YMax}, ScaleAdjustedPoints),
            finished;
        false -> continue
    end.

scale_adjust_points(PointsXY) ->
    MinPoint = find_min_xy(PointsXY),
    lists:map(fun(Point) -> scale_point(Point, MinPoint) end, PointsXY).

scale_point({X, Y}, {XMin, YMin}) ->
    {X-XMin, Y-YMin}.

find_min_xy(Points) ->
    lists:foldl(fun({X, Y}, {XMin, YMin}) -> {erlang:min(X, XMin), erlang:min(Y, YMin)} end,
                erlang:hd(Points),
                Points).

find_max_xy(ScaleAdjustedPoints) ->
    {get_max_x(ScaleAdjustedPoints),
     get_max_y(ScaleAdjustedPoints)}.

get_max_x(Points) ->
    {StartX, _}=erlang:hd(Points),
    lists:foldl(fun({X, _}, XMax) -> erlang:max(X, XMax) end,
                StartX,
                Points).

get_max_y(Points) ->
    {_, StartY}=erlang:hd(Points),
    lists:foldl(fun({_, Y}, YMax) -> erlang:max(Y, YMax) end,
                StartY,
                Points).

display_points({XMax, YMax}, ScaleAdjustedPoints) ->
    PointSet = sets:from_list(ScaleAdjustedPoints),
    display_rows(0, 0, PointSet, {XMax+1, YMax+1}).

display_rows(XMax, Y, Points, {XMax, YMax}) ->
    io:format("~n"),
    display_rows(0, Y+1, Points, {XMax, YMax});
display_rows(_X, YMax, _Points, {_XMax, YMax}) ->
    io:format("~n");
display_rows(X, Y, Points, XYMax) ->
    Point = case sets:is_element({X, Y}, Points) of
                true  -> "X";
                false -> " "
            end,
    io:format("~s", [Point]),
    display_rows(X+1, Y, Points, XYMax).
