-module(marble_mania).
-export([start/0]).

start() ->
    {Players, LastMarble} = read_data("2018_day_9_input.txt"),
    Part1 = play_game(Players, LastMarble),
    io:format("Part 1: ~w~n", [Part1]),
    Part2 = play_game(Players, LastMarble*100),
    io:format("Part 2: ~w~n", [Part2]).

read_data(Filename) ->
    case file:open(Filename, [read]) of
        {ok, Device} -> try read_line(Device, "") of
                            Data -> Data
                        after file:close(Device)
                        end;
        {error, _}   -> error
    end.

read_line(Device, DataRead) ->
    case io:get_line(Device, "") of
        eof  ->
            DataRead;
        Line ->
            Trimmed_Line = string:trim(Line),
            {match, [Players, LastMarble]} = re:run(Trimmed_Line,
                                                    "^(.*)\s.+;\s.+\s.+\s.+\sworth\s(.*)\s.+$",
                                                    [{capture, [1, 2], list}, ungreedy]),
            {P, _} = string:to_integer(Players),
            {M, _} = string:to_integer(LastMarble),
            {P, M}
    end.

play_game(Players, LastMarble) ->
    Circle = queue:cons(0,
                        queue:new()),
    play_turn(lists:duplicate(Players, 0), Circle, 1, LastMarble+1).

play_turn(Players, _Circle, End, End) ->
    lists:max(Players);
play_turn([CurrentPlayer|Players], Circle, Turn, End) ->
    if
        (Turn rem 23) == 0 -> 
            {NewCurrentPlayer, NewCircle} = counter_clockwise(Circle, CurrentPlayer, Turn);
        true -> 
            NewCircle = clockwise(Circle, Turn),
            NewCurrentPlayer = CurrentPlayer
    end,
    play_turn(Players++[NewCurrentPlayer], NewCircle, Turn+1, End).

clockwise(Circle, Turn) ->
    insert_marble(Circle, Turn, 2).

insert_marble(Circle, Turn, 0) ->
    queue:cons(Turn, Circle);
insert_marble(Circle, Turn, ClockwiseStep) ->
    {{value, Marble}, NewCircle} = queue:out(Circle),
    insert_marble(queue:in(Marble, NewCircle),
                  Turn,
                  ClockwiseStep-1).

counter_clockwise(Circle, CurrentPlayer, Turn) ->
    {Marble, ClockwiseAdjustedCircle} = move_counter_clockwise(Circle, 7),
    {CurrentPlayer+Marble+Turn, ClockwiseAdjustedCircle}.

move_counter_clockwise(Circle, 1) ->
    {{value, Marble}, NewCircle} = queue:out_r(Circle),
    {Marble, NewCircle};
move_counter_clockwise(Circle, CounterClockwiseStep) ->
    {{value, Marble}, NewCircle} = queue:out_r(Circle),
    move_counter_clockwise(queue:in_r(Marble, NewCircle),
                           CounterClockwiseStep-1).

