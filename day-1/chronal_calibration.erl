-module(chronal_calibration).
-export([start/0]).

start() ->
    Data = read_file("2018_day_1_input.txt"),
    Sum = part_1(Data),
    Already_Visited = part_2(Data),
    io:format("Part 1: ~w~nPart 2: ~w~n", [Sum, Already_Visited]).

%% Read data from file
read_file(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Data2 = binary:split(Data, [<<"\n">>], [global]),
    [binary_to_integer(L) || L <- remove_blank(Data2)].

remove_blank(List) ->
    lists:filter(fun(El) -> El /= <<>> end, List).

%% Part 1
part_1(Data) ->
    frequency_sum(Data).

frequency_sum(List) ->
    frequency_sum(List, 0).

frequency_sum([H|T], Sum) ->
    frequency_sum(T, H+Sum);
frequency_sum([], Sum) ->
    Sum.

%% Part 2
part_2(Data)->
    find_already_visited_freq(Data, 0, [0], Data).

find_already_visited_freq([], Current_Freq, Visited_Freqs_List, InitialList) ->
    find_already_visited_freq(InitialList, Current_Freq, Visited_Freqs_List, InitialList);
find_already_visited_freq([H|T], Current_Freq, Visited_Freqs_List, InitialList) ->
    New_Freq = Current_Freq + H,
    case lists:member(New_Freq, Visited_Freqs_List) of
        true  -> New_Freq;
        false -> find_already_visited_freq(T, New_Freq, [New_Freq|Visited_Freqs_List], InitialList)
    end.
