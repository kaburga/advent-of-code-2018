-module(chronal_coordinates).

-export([start/0]).

start() ->
    Coordinates = read_file("2018_day_6_input.txt"),
    {XY_Limits, Grid} = make_grid_from_coords(Coordinates),
    Largest_Area = find_largest_area(XY_Limits, Grid, Coordinates),
    Region_Size = find_desired_region(Grid, Coordinates),
    io:format("Part 1: ~w~nPart 2: ~w~n", [Largest_Area, Region_Size]).

read_file(Filename) ->
    case file:open(Filename, [read]) of
        {ok, Device} -> try get_all_lines(Device, []) of
                            Data -> Data
                        after file:close(Device)
                        end;
        {error, _} -> error
    end.

get_all_lines(Device, List) ->
    case io:get_line(Device, "") of
        eof  -> 
            lists:reverse(List);
        Line -> 
            Stripped_Line = string:trim(Line),
            {match, [X, Y]} = re:run(Stripped_Line, 
                                             "(\\d+),\s(\\d+)$",
                                             [{capture, [1, 2], list}, ungreedy]),
            XInt = erlang:element(1, string:to_integer(X)),
            YInt = erlang:element(1, string:to_integer(Y)),
            get_all_lines(Device, [{XInt, YInt}|List])
    end.

make_grid_from_coords(Coordinates) ->
    Sorted_By_X = lists:sort(fun(A, B) -> {A1, _}=A, {B1, _}=B, A1=<B1 end, Coordinates),
    Sorted_By_Y = lists:sort(fun(A, B) -> {_, A2}=A, {_, B2}=B, A2=<B2 end, Coordinates),
    Smallest_X = erlang:element(1, erlang:hd(Sorted_By_X)) - 1,
    Largest_X  = erlang:element(1, lists:last(Sorted_By_X))+ 1,
    Smallest_Y = erlang:element(2, erlang:hd(Sorted_By_Y)) - 1,
    Largest_Y  = erlang:element(2, lists:last(Sorted_By_Y))+ 1,
    X_List = lists:seq(Smallest_X, Largest_X),
    Y_List = lists:seq(Smallest_Y, Largest_Y),
    Grid = lists:flatten(
             lists:map(fun(Y) ->
                               lists:map(fun(X) -> 
                                                 {X, Y} 
                                         end, X_List)
                       end, Y_List)),
    {[Smallest_X, Largest_X, Smallest_Y, Largest_Y], Grid}.

%% Part 1
find_largest_area(XY_Limits, Grid, Coordinates) ->
    Self = self(),
    Loc_Pids = [spawn_link(fun() -> 
                                   Self ! {self(), locate_closest_coordinate(XY_Limits, Location, Coordinates)} 
                           end)
                || Location <- Grid],
    Closest_Coordinates_Per_Location = [receive {Loc_Pid, R} -> R end || Loc_Pid <- Loc_Pids],
    Closest_Coordinates_None_Removed = remove_closest_to_none(Closest_Coordinates_Per_Location),
    Coords_With_Infinite_Area = find_coords_with_infinite_area(Closest_Coordinates_None_Removed),
    Closest_Coordinates_Inf_Removed = remove_locations_with_inf_area_coord(Closest_Coordinates_None_Removed,
                                                                           Coords_With_Infinite_Area),
    Coords_Inf_Removed = remove_from_list(Coords_With_Infinite_Area, Coordinates),
    Coord_Pids = [spawn_link(fun() -> 
                                     Self ! {self(), number_of_times_coord_is_closest(Coordinate, Closest_Coordinates_Inf_Removed)} 
                             end)
                  || Coordinate <- Coords_Inf_Removed],
    lists:last(
      lists:sort([receive {Coord_Pid, R} -> R end || Coord_Pid <- Coord_Pids])).

remove_from_list([Elem|Rest], L) ->
    remove_from_list(Rest, lists:delete(Elem, L));
remove_from_list([], L) ->
    L.

remove_closest_to_none(Closest_Coordinates_Per_Location) ->
    lists:filter(fun(Elem) ->
                         case Elem of
                             {_, none} -> false;
                             _ -> true
                             end
                 end,
                 Closest_Coordinates_Per_Location).

find_coords_with_infinite_area(Closest_Coordinates_None_Removed) ->
    lists:usort(
      lists:filtermap(fun(Elem) ->
                              case Elem of
                                  {_, {Coord, inf}} -> {true, Coord};
                                  {_, _} -> false
                              end
                      end, 
                      Closest_Coordinates_None_Removed)).

remove_locations_with_inf_area_coord(Closest_Coordinates_None_Removed, Coords_With_Infinite_Area) ->
    lists:filter(fun(Elem) ->
                         case Elem of
                             {_, {Coord, inf}} ->
                                 Member = lists:member(Coord, Coords_With_Infinite_Area),
                                 if
                                     Member == true -> false;
                                     true -> true
                                 end;
                             {_, Coord} ->
                                 Member = lists:member(Coord, Coords_With_Infinite_Area),
                                 if
                                     Member == true -> false;
                                     true -> true
                                 end
                         end
                 end,
                 Closest_Coordinates_None_Removed).

number_of_times_coord_is_closest(Coordinate, List_Of_Locations) ->
    erlang:length(
      lists:filter(fun(Elem) -> 
                           {_, Closest_Coord}=Elem,
                           if
                               Closest_Coord == Coordinate -> true;
                               true -> false
                           end
                   end, List_Of_Locations)).

locate_closest_coordinate(XY_Limits, Location, Coordinates) ->
    {X1, Y1} = Location,
    Find_Closest = fun({X2, Y2}, Closest) ->
                           Distance = erlang:abs(X1-X2) + erlang:abs(Y1-Y2), 
                           case Closest of
                               {{Coord, Closest_Distance}, none} -> 
                                   if
                                       Distance < Closest_Distance -> {{X2, Y2}, Distance};
                                       Distance == Closest_Distance -> {{Coord, Closest_Distance}, none};
                                       true -> Closest
                                   end;
                               {Coord, Closest_Distance} -> 
                                   if
                                       Distance < Closest_Distance -> {{X2, Y2}, Distance};
                                       Distance == Closest_Distance -> {{Coord, Closest_Distance}, none};
                                       true -> Closest
                                   end
                           end
                   end,
    Closest_Coord = lists:foldl(Find_Closest, {nothing, {infNum}}, Coordinates),
    case Closest_Coord of
        {_, none} -> 
            {Location, none};
        {Coord, _} ->
            XMember = lists:member(X1, XY_Limits),
            YMember = lists:member(Y1, XY_Limits),
            if
                XMember == true -> {Location, {Coord, inf}};
                YMember == true -> {Location, {Coord, inf}};
                true -> {Location, Coord}
            end
    end.

%% Part 2
find_desired_region(Grid, Coordinates) ->
    Self = self(),    
    Pids = [spawn_link(fun() -> 
                               Self ! {self(), sum_distance_to_coordinates(Location, Coordinates)} 
                       end)
            || Location <- Grid],
    Total_Distance_To_Coords_Per_Location = [receive {Pid, R} -> R end || Pid <- Pids],
    erlang:length(
      lists:filter(fun(Elem) ->
                         if
                             Elem >= 10000 -> false;
                             true -> true
                         end

                 end , 
                   Total_Distance_To_Coords_Per_Location)).

sum_distance_to_coordinates(Location, Coordinates) ->
    {X1, Y1} = Location,
    lists:foldl(fun({X2, Y2}, Sum) ->
                        Distance = erlang:abs(X1-X2) + erlang:abs(Y1-Y2),
                        Sum+Distance
                end, 0, Coordinates).

