-module(memory_maneuver).
-export([start/0]).

-record(node, {children=0, metadata=0, child_list=[], metadata_list=[]}).

start() ->
    InputData = read_data("2018_day_8_input.txt"),
    {Tree, _} = build_tree(InputData),
    Part1 = sum_metadata(Tree),
    io:format("Part 1: ~w~n", [Part1]),
    Part2 = node_value(Tree),
    io:format("Part 2: ~w~n", [Part2]).

read_data(Filename) ->
    case file:open(Filename, [read]) of
        {ok, Device} -> try read_line(Device, "") of
                            Data -> Data
                        after file:close(Device)
                        end;
        {error, _} -> error
    end.

read_line(Device, DataRead) ->
    case io:get_line(Device, "") of
        eof ->
            [erlang:list_to_integer(X) || X <- string:tokens(DataRead, " ")];
        Line ->
            StrippedLine = string:trim(Line),
            read_line(Device, StrippedLine)
    end.

build_tree([ChildCount, MetadataCount | Tail]) ->
    {ChildList, ReturnList} = make_child_node_list(ChildCount, Tail),
    {MetadataList, NewTail} = lists:split(MetadataCount, ReturnList),
    Node = #node{children = ChildCount,
                 metadata = MetadataCount,
                 child_list = ChildList,
                 metadata_list = MetadataList},
    {Node, NewTail}.

make_child_node_list(N, StartList) ->
    make_child_node_list(N, StartList, []).
make_child_node_list(0, StartList, ChildList) ->
    {lists:reverse(ChildList), StartList};
make_child_node_list(N, StartList, ChildList) ->
    {Node, ReturnList} = build_tree(StartList),
    make_child_node_list(N-1, ReturnList, [Node|ChildList]).

%% Part 1
sum_metadata(Node) ->
    MetadataSum = lists:foldl(fun(X, Acc) ->
                                      Acc + X
                              end, 0, Node#node.metadata_list),
    case Node#node.children of
        0 -> MetadataSum;
        _ -> MetadataSum + sum_metadata_childnodes(Node#node.child_list)
    end.

sum_metadata_childnodes(Nodes) ->
    sum_metadata_childnodes(Nodes, 0).
sum_metadata_childnodes([Node|Rest], Sum) ->
    sum_metadata_childnodes(Rest, Sum + sum_metadata(Node));
sum_metadata_childnodes([], Sum) ->
    Sum.

%% Part 2
node_value(Node) ->
    case Node#node.children of
        0 -> 
            lists:foldl(fun(X, Acc) ->
                                Acc + X
                        end, 0, Node#node.metadata_list);
        _ -> 
            L = [node_value(lists:nth(X, Node#node.child_list)) || X <- Node#node.metadata_list, X>0, X=<Node#node.children],
            lists:foldl(fun(X, Acc) ->
                                Acc + X
                        end, 0, L)
    end.    

