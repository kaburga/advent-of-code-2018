-module(alchemical_reduction).
-export([start/0]).

start() ->
    Data = read_file("2018_day_5_input.txt"),
    Processed_Polymer = trigger_reaction(Data),
    Part_1 = erlang:length(Processed_Polymer),
    Part_2 = improve_polymer(Data),
    io:format("Part 1: ~w~nPart 2: ~w~n", [Part_1, Part_2]).

read_file(Filename) ->
    case file:open(Filename, [read]) of
        {ok, Device} -> try get_all_lines(Device, []) of
                            Data -> string:trim(Data)
                        after file:close(Device)
                        end;
        {error, _} -> error
    end.

get_all_lines(Device, List) ->
    case io:get_line(Device, "") of
        eof  -> 
            lists:reverse(List);
        Line -> 
            get_all_lines(Device, [Line|List])
    end.

trigger_reaction(Polymer) ->
    {Processed_Polymer, Reaction} = trigger_reaction([], Polymer, false),
    if
        Reaction == false -> Processed_Polymer;
        Reaction == true  -> trigger_reaction(Processed_Polymer)
    end.

trigger_reaction(Processed_Polymer, [], Reaction) ->
    {lists:reverse(Processed_Polymer), Reaction};
trigger_reaction(Processed_Polymer, Polymer, Reaction) ->
    [First|Tail1] = Polymer,
    if
        Tail1 == [] -> 
            trigger_reaction([First|Processed_Polymer], Tail1, Reaction);
        true ->
            [Second|Tail2] = Tail1,
            Uppercase_First = erlang:hd(string:uppercase([First])),
            Lowercase_First = erlang:hd(string:lowercase([First])),
            Uppercase_Second = erlang:hd(string:uppercase([Second])),
            Lowercase_Second = erlang:hd(string:lowercase([Second])),
            case Uppercase_First == Uppercase_Second of
                true ->
                    if
                        Uppercase_First == First, Lowercase_Second == Second ->
                            trigger_reaction(Processed_Polymer, Tail2, true);
                        Lowercase_First == First, Uppercase_Second == Second ->
                            trigger_reaction(Processed_Polymer, Tail2, true);
                        true -> 
                            trigger_reaction([First|Processed_Polymer], Tail1, Reaction)
                    end;
                false ->
                    trigger_reaction([First|Processed_Polymer], Tail1, Reaction)
            end
    end.

%% Part 2
improve_polymer(Polymer) ->    
    Self = self(),
    Pids = [spawn_link(fun() -> Self ! {self(), react_improved_polymer(X, Polymer)} end)
             || X <- lists:seq($a, $z)],
    List_Of_Improved_Polymers_Length = [receive {Pid, R} -> R end || Pid <- Pids],
    erlang:hd(lists:sort(List_Of_Improved_Polymers_Length)).

react_improved_polymer(X, Polymer) ->
    Filterfun = fun(Ch) -> 
                        Uppercase_X = erlang:hd(string:uppercase([X])),
                        if
                            Ch == X -> false;
                            Ch == Uppercase_X -> false;
                            true -> true
                        end
                end,
    Filtered_Polymer = lists:filter(Filterfun, Polymer),
    Processed_Filtered_Polymer = trigger_reaction(Filtered_Polymer),
    erlang:length(Processed_Filtered_Polymer).
                                               
