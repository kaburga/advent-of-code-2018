-module(inventory_management_system).
-export([start/0]).

-define(Alphabet, "abcdefghijklmnopqrstuvwxyz").

start() ->
    Data = read_file("2018_day_2_input.txt"),
    Checksum = part_1(Data),
    CommonLetters = part_2(Data),
    io:format("Part 1: ~w~nPart 2: ~s~n", [Checksum, CommonLetters]).

%% Read data from file
read_file(Filename) ->
    {ok, Data} = file:read_file(Filename),
    Data2 = binary:split(Data, [<<"\n">>], [global]),
    [binary_to_list(L) || L <- remove_blank(Data2)].

remove_blank(List) ->
    lists:filter(fun(El) -> El /= <<>> end, List).

%% Part 1
part_1(Data)->
    calculate_checksum(Data).

calculate_checksum(Data) ->
    FoundInstancesOf2and3 = [find_duplicates_and_triplicates(List) || List <- Data],
    #{2 := Twos, 3 := Threes} = to_map(lists:flatten(FoundInstancesOf2and3)),
    Twos * Threes.

find_duplicates_and_triplicates(List) ->
    Map = to_map(List),
    lists:usort(
      maps:values(
        maps:filter(fun(_K, V)-> (V == 2) or (V == 3) end, Map))).

to_map(List) ->
    MapUpdater = fun(Ch, M)-> maps:update_with(Ch, fun(V)-> V+1 end, 1, M) end,
    lists:foldl(MapUpdater,
                maps:new(),
                List).

%% Part 2
part_2(Data)->
    find_differ_by_one(Data).

find_differ_by_one([]) -> not_found;
find_differ_by_one([H|T]) ->
    case check_if_differ_by_one(H, T) of
        not_found -> find_differ_by_one(T);
        Result -> Result
    end.

check_if_differ_by_one(_Id, []) -> not_found;
check_if_differ_by_one(Id1, [Id2|Ids]) ->
    case check_if_differ_by_one(Id1, Id2, 0, []) of
        not_found -> check_if_differ_by_one(Id1, Ids);
        CommonLetters -> CommonLetters
    end.

check_if_differ_by_one(_, _, 2, _) -> not_found;
check_if_differ_by_one([H1|T1], [H2|T2], Diff, Common) ->
    if
        H1 == H2 -> check_if_differ_by_one(T1, T2, Diff, [H1|Common]);
        H1 /= H2 -> check_if_differ_by_one(T1, T2, Diff+1, Common)
    end;
check_if_differ_by_one([], [], 1, Common) -> lists:reverse(Common);
check_if_differ_by_one([], [], 0, _Common) -> not_found.
