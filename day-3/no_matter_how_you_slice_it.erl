-module(no_matter_how_you_slice_it).
-export([start/0]).

-record(claim, {num, x1, x2, y1, y2}).

start() ->
    Data = read_file("2018_day_3_input.txt"),
    {Part1, Part2} = num_of_overlapping_claims(Data),
    io:format("Part 1: ~w~nPart 2: ~w~n", [Part1, Part2]).

read_file(Filename) ->
    case file:open(Filename, [read]) of
        {ok, Device} -> try get_all_lines(Device, []) of
                            Data -> Data
                        after file:close(Device)
                        end;
        {error, _} -> error
    end.

get_all_lines(Device, List) ->
    case io:get_line(Device, "") of
        eof  -> 
            lists:reverse(List);
        Line ->
            Stripped_Line = string:trim(Line),
            {match, [Num, From_Left, From_Top, Width, Height]} = re:run(Stripped_Line, 
                                                                      "#(.*)\s@\s(.*),(.*):\s(.*)x(.*)$",
                                                                      [{capture, [1, 2, 3, 4, 5], list}, ungreedy]), 
            {X1, _} = string:to_integer(From_Left),
            X2 = (erlang:element(1, string:to_integer(From_Left)) + erlang:element(1, string:to_integer(Width))),
            {Y1, _} = string:to_integer(From_Top),
            Y2 = erlang:element(1,string:to_integer(From_Top)) + erlang:element(1, string:to_integer(Height)),
            Fabric_Claim = #claim{num=erlang:element(1, string:to_integer(Num)),
                                  x1=X1+1,
                                  x2=X2,
                                  y1=Y1+1,
                                  y2=Y2},
            get_all_lines(Device, [Fabric_Claim|List])
    end.

%% Part 1
num_of_overlapping_claims(Data) ->
    num_of_overlapping_claims(Data, []).

num_of_overlapping_claims([H|T], All_Claims) ->
    New_Claims = make_claim_list(H),
    num_of_overlapping_claims(T, lists:append(All_Claims, New_Claims));
num_of_overlapping_claims([], All_Claims)                                         ->
    {Overlapping, Claim_Nums} = remove_nonoverlapping(All_Claims),
    Single_Non_Overlapping_Claim = find_non_overlapping_claim(Claim_Nums),
    {erlang:length(Overlapping), Single_Non_Overlapping_Claim}.

make_claim_list(Claim)                                                            ->
    X_List = lists:seq(Claim#claim.x1, Claim#claim.x2),
    Y_List = lists:seq(Claim#claim.y1, Claim#claim.y2),
    Claim_List = lists:map(fun(Y) -> lists:map(fun(X) -> {Claim#claim.num, {X, Y}} end, X_List) end, Y_List),
    lists:flatten(Claim_List).

remove_nonoverlapping(All_Claims)                                                 ->
    Sorted = lists:sort(fun(A, B) -> {_, A2}=A, {_, B2}=B, A2=<B2  end, All_Claims),
    remove_nonoverlapping(Sorted, [], [], []).

remove_nonoverlapping([H|T], Overlapping, Overlapping_Nums, Non_Overlapping_Nums) ->
    if
        T == [] -> 
            {lists:usort(fun(A, B) -> {_, A2}=A, {_, B2}=B, A2=<B2 end, Overlapping), 
             {Overlapping_Nums, Non_Overlapping_Nums}};
        true    ->     
            {Num1, Coord1} = H,
            [H2|T2] = T,
            {Num2, Coord2} = H2,
            case Coord1 == Coord2 of
                true  -> remove_nonoverlapping(T2, 
                                               [H|Overlapping], 
                                               [[Num1,Num2]|Overlapping_Nums], 
                                               Non_Overlapping_Nums);
                false -> remove_nonoverlapping(T, 
                                               Overlapping, 
                                               Overlapping_Nums, 
                                               [Num1|Non_Overlapping_Nums])
            end 
    end.

%% Part 2
find_non_overlapping_claim({Overlapping_Nums, Non_Overlapping_Nums}) ->
    Filtered_Overlapping_Nums = lists:usort(lists:flatten(Overlapping_Nums)),
    Filtered_Non_Overlapping_Nums = lists:usort(lists:flatten(Non_Overlapping_Nums)),
    find_non_overlapping_claim(Filtered_Overlapping_Nums, Filtered_Non_Overlapping_Nums).

find_non_overlapping_claim(Overlapping_Nums, [H|T]) ->
    case lists:member(H, Overlapping_Nums) of
        true  -> find_non_overlapping_claim(Overlapping_Nums, T);
        false -> H
    end.
    
